package com.trackme.vicky.trackme;

/**
 * Created by vicky on 2/3/15.
 */

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.webkit.JavascriptInterface;
import android.widget.Toast;
import android.app.Activity;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by vicky on 1/20/2015.
 */
public class WebAppInterface {
    Context mContext;
    public PendingIntent pendingIntent;
    public Activity nActiv = new Activity();

    /** Instantiate the interface and set the context */
    WebAppInterface(Context c) {
        mContext = c;
    }

    public static String creds;
    private static boolean flg = false;
    public Timer timer;
    //private AlarmManager alarmMgr;

    /** Show a toast from the web page */

    @JavascriptInterface
    public void setCreds(String toast) {
        //String txt = "GPS Location: "+ MainActivity.gps.Latitude + " : " + MainActivity.gps.Longitude;
        MainActivity.creds = toast;
    }


    @JavascriptInterface
    public String showToast(String toast) {
        //String txt = "GPS Location: "+ MainActivity.gps.Latitude + " : " + MainActivity.gps.Longitude;
        String txt = "GPS Location: "+  String.valueOf(MainActivity.latitude) + " : " +  String.valueOf(MainActivity.longitude);
        Toast.makeText(mContext, txt, Toast.LENGTH_SHORT).show();
        creds = toast;
        //getReq(creds);
        return String.valueOf(MainActivity.latitude)+","+String.valueOf(MainActivity.longitude);
    }

    @JavascriptInterface
    public void toggleUpdates(String toast, String tm) {
        flg = !flg;
        MainActivity.timeron = flg;
        MainActivity.timePeriodMinutes = Integer.parseInt(tm);
        Toast.makeText(mContext, "Toggle hit! time:" + tm, Toast.LENGTH_SHORT).show();
        if (flg == true) {
            Toast.makeText(mContext, "Toggle hit true", Toast.LENGTH_SHORT).show();
        }
    }

    public void getReq(String val)
    {
        StringBuilder url = new StringBuilder("http://www.vikwiz.org/API/access?");
        url.append(val + "&lati=" + String.valueOf(MainActivity.latitude)+"&longi="+String.valueOf(MainActivity.longitude));
        //urlConnection.setRequestProperty("User-Agent", USER_AGENT);
        try {
            URL obj = new URL(url.toString());
            HttpURLConnection urlConnection = (HttpURLConnection) obj.openConnection();
            urlConnection.setRequestMethod("GET");
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(urlConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Toast.makeText(mContext, response, Toast.LENGTH_SHORT).show();
            urlConnection.disconnect();
        }
       catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    public void cancel() {
        AlarmManager manager = (AlarmManager) nActiv.getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        Toast.makeText(mContext, "Updates Canceled", Toast.LENGTH_SHORT).show();
    }

    public void startUpdatefor(int tm) {
        AlarmManager manager = (AlarmManager) nActiv.getSystemService(Context.ALARM_SERVICE);
        int interval = 1000 * 60 * tm;


        try {
            manager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, 5000, interval, pendingIntent);
            Toast.makeText(mContext, "Alarm Manager Set!", Toast.LENGTH_SHORT).show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        Toast.makeText(mContext, "Alarm Manager Set done!", Toast.LENGTH_SHORT).show();
    }
    */
}



