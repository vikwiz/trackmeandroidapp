package com.trackme.vicky.trackme;

import android.content.Intent;
import android.content.Context;


import android.content.BroadcastReceiver;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class LocUpdateService extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        // For our recurring task, we'll just display a message
        try {
            StringBuffer response = new StringBuffer();
            if (MainActivity.timeron == true) {
                Toast.makeText(context, "Sending Coords to http://www.vikwiz.org/API/access?" + MainActivity.creds + "&lati=" + String.valueOf(MainActivity.latitude) + "&longi=" + String.valueOf(MainActivity.longitude), Toast.LENGTH_SHORT).show();
                response = sendData();
                Toast.makeText(context, response, Toast.LENGTH_SHORT).show();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public StringBuffer sendData() {
        StringBuffer response = new StringBuffer();
        if (MainActivity.timeron == true) {
            MainActivity.minutesElapsed = MainActivity.minutesElapsed + 1;
            if (MainActivity.minutesElapsed >= MainActivity.timePeriodMinutes) {
                StringBuilder url = new StringBuilder("http://www.vikwiz.org/API/access?");
                url.append(MainActivity.creds + "&lati=" + String.valueOf(MainActivity.latitude) + "&longi=" + String.valueOf(MainActivity.longitude));
                //urlConnection.setRequestProperty("User-Agent", USER_AGENT);
                try {
                    URL obj = new URL(url.toString());
                    HttpURLConnection urlConnection = (HttpURLConnection) obj.openConnection();
                    urlConnection.setRequestMethod("GET");
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream()));
                    String inputLine;

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                    MainActivity.minutesElapsed = 0;
                    //Toast.makeText(this, response, Toast.LENGTH_SHORT).show();
                    urlConnection.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                MainActivity.minutesElapsed = 0;
            }
        }
        return response;
    }
}